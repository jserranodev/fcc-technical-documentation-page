# fcc-technical-documentation-page

- FreeCodeCamp Responsive Web Design Certification. Project #4: Technical Documentation Page.
- [Project to be solved.](https://www.freecodecamp.org/learn/responsive-web-design/responsive-web-design-projects/build-a-technical-documentation-page)
- [Solution to project.](https://jserranodev.gitlab.io/fcc-technical-documentation-page)
